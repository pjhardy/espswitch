# ESPSwitch #

![picture](single.png) ![picture](dual.png)
Eagle schematics and PCBs for basic ESP12e switches. These boards feature:

* Linear regulator to power the ESP8266.
* Programming header, with access to serial, reset and flash pins.
* One or two outputs. When the ESP8266 sets these high, they will
  output Vin to drive a relay or other device.
  
in a cute [Sick of Beige form factor](http://dangerousprototypes.com/docs/Sick_of_Beige).

I've had a run of the single output boards made
(through [OSHPark](https://oshpark.com/)) with no problems. Have not yet
verified the dual-output design.

## License ##

These schematics are available under a [Creative Commons Attribution license](https://creativecommons.org/licenses/by/4.0/).
